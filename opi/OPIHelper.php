<?php
/**
 * Little helper class to request a payment on an OPI enabled Card Terminal
 */
class OPIHelper {

   private $ip;
   private $port;
   private $requestID = 0;

   private function requestID()
   {
      return $this->requestID++;
   }

   private function loginXML()
   {
      return "
      <?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
      <ServiceRequest RequestType=\"Login\" WorkstationID=\"0\" RequestID=\"".$this->requestID()."\">
         <POSdata>
            <POSTimeStamp>".date("c")."</POSTimeStamp>
         </POSdata>
      </ServiceRequest>";
   }

   private function reconciliationXML()
   {
      return "
      <?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
      <ServiceRequest RequestType=\"Reconciliation\" WorkstationID=\"0\" RequestID=\"".$this->requestID()."\">
         <POSdata>
            <POSTimeStamp>".date("c")."</POSTimeStamp>
         </POSdata>
      </ServiceRequest>";
   }

   private function logoutXML()
   {
      return "
      <?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
      <ServiceRequest RequestType=\"Logoff\" WorkstationID=\"0\" RequestID=\"".$this->requestID()."\">
         <POSdata>
            <POSTimeStamp>".date("c")."</POSTimeStamp>
         </POSdata>
      </ServiceRequest>";
   }

   private function cardPaymentXML($amount)
   {
      return "
         <?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
         <CardServiceRequest RequestType=\"CardPayment\" WorkstationID=\"0\" RequestID=\"".$this->requestID()."\">
            <POSdata>
               <POSTimeStamp>".date("c")."</POSTimeStamp>
            </POSdata>
            <TotalAmount>$amount</TotalAmount>
         </CardServiceRequest>";
   }

   private function ticketReprintXML($originalTransaction = null)
   {
      return "
         <?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
         <CardServiceRequest RequestType=\"TicketReprint\" WorkstationID=\"0\" RequestID=\"".$this->requestID()."\">
            <POSdata>
               <POSTimeStamp>".date("c")."</POSTimeStamp>
            </POSdata>
         </CardServiceRequest>";
      //TODO: <OriginalTransaction TerminalID="" TerminalBatch="" STAN="" TimeStamp""/>
   }

   private function receiveXML($f)
   {
      $length = unpack("Ni", fread($f, 4));
      $data = fread($f, $length["i"]);
      return $data;
   }

   private function sendXML($data, $f)
   {
      fwrite($f, pack("N", strlen($data)));
      fwrite($f, $data);
      echo "Sending...\n";
      echo $data;
      return $this->receiveXML($f);
   }

   public function __construct($ip = "192.168.1.213", $port = 4100)
   {
      $this->ip = $ip;
      $this->port = $port;
   }

   public function requestMoney($amount)
   {
      $result = false;
      $f = fsockopen($this->ip, $this->port);
      $data = $this->sendXML($this->loginXML(), $f);
      if (strpos($data, 'OverallResult="Success"') === false)
      {
         echo "Error during login\n";
         echo $data;
         fclose($f);
         return false;
      }
      else
      {
        echo "Login successful\n";
      }
      $data = $this->sendXML($this->cardPaymentXML($amount), $f);
      if (strpos($data, 'OverallResult="Aborted"') !== false)
      {
         echo "Payment Aborted\n";
      }
      else if (strpos($data, 'OverallResult="Success"') !== false)
      {
         $result = true;
      }
      $data = $this->sendXML($this->logoutXML(), $f);
      if (strpos($data, 'OverallResult="Success"') === false)
      {
         echo "Error during logout\n";
         echo $data;
      }
      fclose($f);
      return $result;
   }

   public function reprintTicket()
   {
      $result = false;
      $f = fsockopen($this->ip, $this->port);
      $data = $this->sendXML($this->loginXML(), $f);
      if (strpos($data, 'OverallResult="Success"') === false)
      {
         echo "Error during login\n";
         echo $data;
         fclose($f);
         return false;
      }
      $data = $this->sendXML($this->ticketReprintXML(), $f);
      if (strpos($data, 'OverallResult="Success"') !== false)
      {
         $result = true;
      }
      $data = $this->sendXML($this->logoutXML(), $f);
      if (strpos($data, 'OverallResult="Success"') === false)
      {
         echo "Error during logout\n";
         echo $data;
      }
      fclose($f);
      return $result;
   }

   public function listPayments()
   {
      $result = false;
      $f = fsockopen($this->ip, $this->port);
      $data = $this->sendXML($this->loginXML(), $f);
      if (strpos($data, 'OverallResult="Success"') === false)
      {
         echo "Error during login\n";
         echo $data;
         fclose($f);
         return false;
      }
      $data = $this->sendXML($this->reconciliationXML(), $f);

      $xml = new SimpleXMLElement($data);
      if ($xml["OverallResult"] == "Success")
      {
         $result = array();
         foreach ($xml->Reconciliation as $r)
         {
            foreach ($r->TotalAmount as $ta)
            {
               $r = array();
               foreach (array("NumberPayments","PaymentType","Currency","CardCircuit","Acquirer") as $field)
               {
                  if (isset($ta[$field]) && $ta[$field] !== "")
                  {
                     $r[$field] = $ta[$field];
                  }
               }
               $result []= $r;
            }
         }
      }
      $data = $this->sendXML($this->logoutXML(), $f);
      if (strpos($data, 'OverallResult="Success"') === false)
      {
         echo "Error during logout\n";
         echo $data;
      }
      fclose($f);
      return $result;
   }

   public function test()
   {
     $result = false;
     $f = fsockopen($this->ip, $this->port);

     $data = $this->sendXML($this->loginXML(), $f);
     if (strpos($data, 'OverallResult="Success"') === false)
     {
        echo "Error during login\n";
        echo $data;
        fclose($f);
        return false;
     }
     else
     {
       echo "Login successful\n";
       echo $data;
     }

     // $data = $this->sendXML($this->logoutXML(), $f);
     // if (strpos($data, 'OverallResult="Success"') === false)
     // {
     //    echo "Error during logout\n";
     //    echo $data;
     // }
     // else
     // {
     //   echo "Logout successful\n";
     // }

     fclose($f);
     return $result;

   }
}

$opiClient = new OPIHelper();
// $opiClient->listPayments();
// $opiClient->reprintTicket();
$opiClient->requestMoney(2.0);
// $opiClient->test();

?>
