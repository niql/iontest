const net = require('net')
const moment = require('moment')
const parser = require('fast-xml-parser')
const parserOpts = {
  attributeNamePrefix : "",
  attrNodeName: "attr",
  textNodeName : "text",
  trimValues: false,
  ignoreAttributes : false,
  ignoreNameSpace : false,
  allowBooleanAttributes : true,
  parseNodeValue : true,
  parseAttributeValue : true,
}

const Terminal = '192.168.1.213'
const TXPORT = '4100'
const RXPORT = '4102'

var server = net.createServer()
var client = new net.Socket()

function SaleNormal(id, amount) {
  return `<?xml version="1.0" encoding="utf-8"?>
<CardServiceRequest WorkstationID="POS" RequestID="${id.toString()}" RequestType="CardPayment" xmlns="http://www.nrf-arts.org/IXRetail/namespace">
  <POSdata LanguageCode="nl">
    <POSTimeStamp>${moment().format()}</POSTimeStamp>
    <ShiftNumber>123</ShiftNumber>
    <PrinterStatus>Available</PrinterStatus>
    <E-JournalStatus>Available</E-JournalStatus>
    <JournalPrinterStatus>Available</JournalPrinterStatus>
  </POSdata>
  <TotalAmount Currency="EUR">${(amount/100.0).toFixed(2)}</TotalAmount>
  <CardCircuitCollection>
  </CardCircuitCollection>
</CardServiceRequest>
`
}

function DeviceResponse(id, target) {
  return `<?xml version="1.0" encoding="utf-8"?>
<DeviceResponse WorkstationID="POS" RequestID="${id.toString()}" RequestType="Output" OverallResult="Success" xmlns="http://www.nrf-arts.org/IXRetail/namespace">
  <Output OutDeviceTarget="${target}" OutResult="Success">
  </Output>
</DeviceResponse>
`
}

function MesssageTx(mesg) {
  console.log(mesg)

  let payload = Buffer.from(mesg)
  // console.log(payload)
  let header = Buffer.allocUnsafe(4)
  header.writeUInt32BE(mesg.length)
  // console.log(header)

  return Buffer.concat([header, payload])
}

function MesssageRX(socket) {
  let header = socket.read(4)
  if(!header) {
    // incomplete header
    return null
  }
  // console.log(header)

  let payloadSize = header.readUInt32BE(0)
  let payload = socket.read(payloadSize)
  if(!payload) {
    // incomplete payload, un-read header
    socket.unshift(header)
    return null
  }
  // console.log(payloadSize)

  let mesg = payload.toString('utf8')
  console.log(mesg)
  return mesg
}

function cleanup(code) {
  client.destroy()
  server.close(() => {
    process.exit(code)
  })
}

server.on('connection', (socket) => {
  socket.on('readable', () => {
    let mesg = MesssageRX(socket)
    if(!mesg) return

    let mesgObj = parser.parse(mesg, parserOpts)
    // console.log(require('util').inspect(mesgObj, false,null,false))

    let id = mesgObj.DeviceRequest.attr.RequestID
    let target = mesgObj.DeviceRequest.Output.attr.OutDeviceTarget
    let response = MesssageTx(DeviceResponse(id, target))

    socket.write(response)
  })

  socket.on('error', (err) => {
    console.log(`onError: ${err}`)
  })
})

server.on('error', (e) => {
  if (e.code === 'EADDRINUSE') {
    console.log('Address in use, retrying...')
    setTimeout(() => {
      server.close()
      server.listen(RXPORT)
    }, 3000)
  } else {
    cleanup(-1)
  }
})

client.on('readable', () => {
  let mesg = MesssageRX(client)
  if(!mesg) return
  // last message in the transaction
  cleanup(0)
})

client.on('error', (err) => {
  console.log(err)
  cleanup(-1)
})

server.listen(RXPORT, () => {
  console.log(`Server listening on ${RXPORT}`)
})

client.connect(TXPORT, Terminal, () => {
	// console.log('Connected')
  let sale = MesssageTx(SaleNormal(1000,50))
  client.write(sale)
})
